#ifndef STREAM9_LINUX_PROC_TEST_SRC_JSON_HANDLER_HPP
#define STREAM9_LINUX_PROC_TEST_SRC_JSON_HANDLER_HPP

#include "namespace.hpp"

#include <stream9/linux/proc/mountinfo.hpp>

#include <string_view>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>

namespace testing {

struct json_handler
{
    using iterator = std::string_view::iterator;

    json::array m_entries;
    json::array m_errors;

    std::string_view m_text;
    int m_line_no = 1;
    iterator m_bol;

    json::object m_entry;
    json::array m_options;

    json_handler(std::string_view s)
        : m_text { s }
        , m_bol { s.begin() }
    {}

    void begin_entry(iterator)
    {
        m_entry = {};
    }

    void on_mount_id(std::string_view s)
    {
        m_entry["mount_id"] = s;
    }

    void on_parent_id(std::string_view s)
    {
        m_entry["parent_id"] = s;
    }

    void on_device_id(std::string_view s)
    {
        m_entry["device_id"] = s;
    }

    void on_root(std::string_view s)
    {
        m_entry["root"] = s;
    }

    void on_mount_point(std::string_view s)
    {
        m_entry["mount_point"] = s;
    }

    void on_mount_options(std::string_view)
    {
        m_entry["mount_options"] = m_options;
        m_options = {};
    }

    void on_mount_option(std::string_view s)
    {
        m_options.push_back(s);
    }

    void on_optional_fields(std::string_view)
    {
        m_entry["optional_fields"] = m_options;
        m_options = {};
    }

    void on_optional_field(std::string_view s)
    {
        m_options.push_back(s);
    }

    void on_filesystem_type(std::string_view s)
    {
        m_entry["filesystem_type"] = s;
    }

    void on_mount_source(std::string_view s)
    {
        m_entry["mount_source"] = s;
    }

    void on_super_options(std::string_view)
    {
        m_entry["super_options"] = m_options;
        m_options = {};
    }

    void end_entry(iterator)
    {
        m_entries.push_back(m_entry);
    }

    void on_error(std::error_code const e,
                  std::string_view::const_iterator it,
                  std::source_location)
    {
        m_errors.push_back(json::object {
            { "message", e.message() },
            { "line no", m_line_no },
            { "column", it - m_bol },
        });
    }

    void on_new_line(iterator it)
    {
        ++m_line_no;
        m_bol = it;
    }
};

} // namespace testing

#endif // STREAM9_LINUX_PROC_TEST_SRC_JSON_HANDLER_HPP
