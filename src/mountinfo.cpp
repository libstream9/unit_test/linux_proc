#include <stream9/linux/proc/mountinfo.hpp>

#include "json_handler.hpp"

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>
#include <stream9/filesystem.hpp>

namespace testing {

namespace mountinfo = stream9::linux::proc::mountinfo;

BOOST_AUTO_TEST_SUITE(mountinfo_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        try {
            auto s = "";
            json_handler h { s };

            auto r = mountinfo::parse(s, h);
            BOOST_REQUIRE(r);

            BOOST_TEST(h.m_entries.empty());
        }
        catch (...) {
            stream9::errors::print_error();
        }
    }

    BOOST_AUTO_TEST_CASE(begin_entry_)
    {
        std::string_view s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

        struct handler {
            using iterator = std::string_view::const_iterator;

            std::string_view s;
            json::array result;

            handler(std::string_view s) : s { s } {}

            void begin_entry(iterator it)
            {
                result.push_back(it - s.begin());
            }
        } h { s };

        mountinfo::parse(s, h);

        json::array expected { 0 };

        BOOST_TEST(h.result == expected);
    }

    BOOST_AUTO_TEST_SUITE(mount_id_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_mount_id(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "36" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(error_1_)
        {
            auto s = "\n36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                std::string_view text;
                json::array result;
                json::array errors;

                handler(std::string_view s) : text { s } {}

                void on_mount_id(std::string_view s)
                {
                    result.push_back(s);
                }

                void on_error(std::error_code e,
                              std::string_view::const_iterator it,
                              std::source_location)
                {
                    errors.push_back(json::array {
                        e.message(),
                        it - text.begin(),
                    });
                }
            } h { s };

            auto r = mountinfo::parse(s, h);
            BOOST_REQUIRE(!r);

            BOOST_TEST(h.result.empty());

            json::array expected {
                { "premature end of line", 0 }
            };

            BOOST_TEST(h.errors == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // mount_id_

    BOOST_AUTO_TEST_SUITE(parent_id_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_parent_id(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "35" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(error_1_)
        {
            auto s = "36\n";

            struct handler {
                std::string_view text;
                json::array result;
                json::array errors;

                handler(std::string_view s) : text { s } {}

                void on_parent_id(std::string_view s)
                {
                    result.push_back(s);
                }

                void on_error(std::error_code e,
                              std::string_view::const_iterator it,
                              std::source_location)
                {
                    errors.push_back(json::array {
                        e.message(),
                        it - text.begin(),
                    });
                }
            } h { s };

            auto r = mountinfo::parse(s, h);
            BOOST_REQUIRE(!r);

            BOOST_TEST(h.result.empty());

            json::array expected {
                { "premature end of line", 2 }
            };

            BOOST_TEST(h.errors == expected);
        }

        BOOST_AUTO_TEST_CASE(error_2_)
        {
            auto s = "36";

            struct handler {
                std::string_view text;
                json::array result;
                json::array errors;

                handler(std::string_view s) : text { s } {}

                void on_parent_id(std::string_view s)
                {
                    result.push_back(s);
                }

                void on_error(std::error_code e,
                              std::string_view::const_iterator it,
                              std::source_location)
                {
                    errors.push_back(json::array {
                        e.message(),
                        it - text.begin(),
                    });
                }
            } h { s };

            auto r = mountinfo::parse(s, h);
            BOOST_REQUIRE(!r);

            BOOST_TEST(h.result.empty());

            json::array expected {
                { "premature end of text", 2 }
            };

            BOOST_TEST(h.errors == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // parent_id_

    BOOST_AUTO_TEST_SUITE(device_id_)

        BOOST_AUTO_TEST_CASE(only_on_device_id_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_device_id(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "98:0" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(only_device_minor_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_device_minor(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "0" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(major_and_minor_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;
                std::string_view major;

                void on_device_id(std::string_view s)
                {
                    major = s;
                }

                void on_device_minor(std::string_view s)
                {
                    assert(!major.empty());

                    result.push_back(json::array { major, s });

                    major = {};
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { { "98", "0" } };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // device_id_

    BOOST_AUTO_TEST_SUITE(root_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_root(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "/mnt1" };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // root_

    BOOST_AUTO_TEST_SUITE(mount_point_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_mount_point(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "/mnt2" };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // mount_point_

    BOOST_AUTO_TEST_SUITE(mount_options_)

        BOOST_AUTO_TEST_CASE(mount_options_1_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_mount_options(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(mount_options_2_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_mount_options(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw,noatime" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(mount_option_1_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_mount_option(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(mount_option_2_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_mount_option(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw", "noatime" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(mount_option_value_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;
                std::string_view option;

                void on_mount_option(std::string_view s)
                {
                    option = s;
                }

                void on_mount_option_value(std::string_view s)
                {
                    BOOST_REQUIRE(!option.empty());

                    result.push_back(json::array { option, s });

                    option = {};
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { { "rw", "" }, { "noatime", "" } };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // mount_options_

    BOOST_AUTO_TEST_SUITE(optional_fields_)

        BOOST_AUTO_TEST_CASE(optional_fields_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 shared:2 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_optional_fields(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "master:1 shared:2" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(optional_field_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 shared:2 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_optional_field(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "master:1", "shared:2" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(optional_field_and_value_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 shared:2 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;
                std::string_view tag;

                void on_optional_field(std::string_view s)
                {
                    tag = s;
                }

                void on_optional_value(std::string_view s)
                {
                    BOOST_REQUIRE(!tag.empty());

                    result.push_back(json::array { tag, s });

                    tag = {};
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected {
                { "master", "1" },
                { "shared", "2" },
            };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // optional_fields_

    BOOST_AUTO_TEST_SUITE(filesystem_type_)

        BOOST_AUTO_TEST_CASE(without_subtype_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 shared:2 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_filesystem_type(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "fuse.encfs" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(with_subtype_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 shared:2 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;
                std::string_view type;

                void on_filesystem_type(std::string_view s)
                {
                    type = s;
                }

                void on_filesystem_subtype(std::string_view s)
                {
                    BOOST_REQUIRE(!type.empty());

                    result.push_back(json::array { type, s });

                    type = {};
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { { "fuse", "encfs" } };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // filesystem_type_

    BOOST_AUTO_TEST_SUITE(mount_source_)

        BOOST_AUTO_TEST_CASE(ok_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 shared:2 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_mount_source(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "/dev/root" };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // mount_source_

    BOOST_AUTO_TEST_SUITE(super_options_)

        BOOST_AUTO_TEST_CASE(super_options_1_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw";

            struct handler {
                json::array result;

                void on_super_options(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(super_options_2_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_super_options(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw,errors=continue" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(super_option_1_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw";

            struct handler {
                json::array result;

                void on_super_option(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(super_option_2_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;

                void on_super_option(std::string_view s)
                {
                    result.push_back(s);
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { "rw", "errors=continue" };

            BOOST_TEST(h.result == expected);
        }

        BOOST_AUTO_TEST_CASE(super_option_value_)
        {
            auto s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

            struct handler {
                json::array result;
                std::string_view option;

                void on_super_option(std::string_view s)
                {
                    option = s;
                }

                void on_super_option_value(std::string_view s)
                {
                    BOOST_REQUIRE(!option.empty());

                    result.push_back(json::array { option, s });

                    option = {};
                }
            } h;

            mountinfo::parse(s, h);

            json::array expected { { "rw", "" }, { "errors", "continue" } };

            BOOST_TEST(h.result == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // super_options_

    BOOST_AUTO_TEST_CASE(end_entry_)
    {
        std::string_view s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue";

        struct handler {
            using iterator = std::string_view::const_iterator;

            std::string_view s;
            json::array result;

            handler(std::string_view s) : s { s } {}

            void end_entry(iterator it)
            {
                result.push_back(it - s.end());
            }
        } h { s };

        mountinfo::parse(s, h);

        json::array expected { 0 };

        BOOST_TEST(h.result == expected);
    }

    BOOST_AUTO_TEST_CASE(exit_)
    {
        std::string_view s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue\n"
                             "37 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue\n"
                             "38 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue\n";

        struct handler {
            json::array ids;

            void on_mount_id(std::string_view s)
            {
                if (s == "37") {
                    throw mountinfo::exit();
                }
                else {
                    ids.push_back(s);
                }
            }
        } h;

        auto r = mountinfo::parse(s, h);
        BOOST_REQUIRE(r);

        json::array expected { "36" };

        BOOST_TEST(h.ids == expected);
    }

    BOOST_AUTO_TEST_CASE(skip_line_)
    {
        std::string_view s = "36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue\n"
                             "37 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue\n"
                             "38 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - fuse.encfs /dev/root rw,errors=continue\n";

        struct handler {
            json::array ids;

            void on_mount_id(std::string_view s)
            {
                if (s == "37") {
                    throw mountinfo::skip_line();
                }
                else {
                    ids.push_back(s);
                }
            }
        } h;

        auto r = mountinfo::parse(s, h);
        BOOST_REQUIRE(r);

        json::array expected { "36", "38" };

        BOOST_TEST(h.ids == expected);
    }

    BOOST_AUTO_TEST_CASE(real_data_)
    {
#if 0
        try {
            using stream9::filesystem::load_string;

            auto s = load_string("/proc/self/mountinfo");
            json_handler h { s };

            mountinfo::parse(s, h);

            std::cout << std::setw(2) << h.m_entries << std::endl;
        }
        catch (...) {
            stream9::errors::print_error();
        }
#endif
    }

BOOST_AUTO_TEST_SUITE_END() // mountinfo_

} // namespace testing
