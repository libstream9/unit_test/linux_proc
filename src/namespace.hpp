#ifndef STREAM9_LINUX_PROC_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_PROC_TEST_SRC_NAMESPACE_HPP

namespace stream9::json {}

namespace testing {

namespace json { using namespace stream9::json; }

} // namespace testing

#endif // STREAM9_LINUX_PROC_TEST_SRC_NAMESPACE_HPP
